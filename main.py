import time
import urllib.request
import location_util
import wallpaper_selector
import wallpaper_updater


def run():
    previous_wallpaper = ''
    try:
        sunrise, sunset = location_util.get_sunset_sunrise()
        wallpaper_name = wallpaper_selector.select_wallpaper(sunrise, sunset)
        if(previous_wallpaper != wallpaper_name):
            wallpaper_updater.set_wallpaper(wallpaper_name)
            previous_wallpaper = wallpaper_name
    except urllib.HTTPError as e:
        logging.warn('HTTPError = ' + str(e.code))
    except urllib2.URLError as e:
        logging.warn('URLError = ' + str(e.reason))
    except httplib.HTTPException as e:
        logging.warn('HTTPException')


def start():
    while True:
        run()
        time.sleep(600)


if __name__ == '__main__':
    start()
