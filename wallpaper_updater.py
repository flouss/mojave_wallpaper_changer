import os
import ctypes


def set_wallpaper(filename):
    filepath = os.path.abspath(os.path.join('wallpapers', filename))
    filepath = os.path.normpath(filepath)
    if(os.name == 'nt'):
        set_wallpaper_windows(filepath)
    else:
        set_wallpaper_linux(filepath)


def set_wallpaper_windows(filepath):
    ''' Set the wallpaper on windows '''
    ctypes.windll.user32.SystemParametersInfoW(20, 0, filepath, 3)


def set_wallpaper_linux(filepath):
    ''' Set the wallpaper on linux '''
    os.system(
        "/usr/bin/gsettings set org.gnome.desktop.background picture-uri {filepath}")
