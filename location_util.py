import re
import json
from datetime import datetime, timedelta
from urllib.request import urlopen

_ip_url = 'http://ipinfo.io/json'
_sunrise_url = 'https://api.sunrise-sunset.org/json?lat={}&lng={}'
_format = '%I:%M:%S %p'


def get_location():
    ''' Get the location according to the ip address '''
    response = urlopen(_ip_url)
    data = json.load(response)
    return data['loc']


def get_sunset_sunrise():
    ''' Get Sunset and sunrise times according to location '''
    location = get_location()
    lat, lng = location.split(',')
    response = urlopen(_sunrise_url.format(lat, lng))
    data = json.load(response)
    sunrise = datetime.strptime(data['results']['sunrise'], _format).time()
    sunset = datetime.strptime(data['results']['sunset'], _format).time()
    # use day length to get a more specific day wallpaper
    return sunrise, sunset
