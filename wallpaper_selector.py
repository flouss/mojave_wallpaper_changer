from datetime import datetime, timedelta


def get_wallpaper_name(suffix):
    ''' Returns wallpaper file name '''
    return f'mojave_{suffix}.jpg'


def select_wallpaper(sunrise, sunset):
    ''' Get the appropriate wallpaper file name according to current time '''
    now = datetime.now().time()
    sunrise_date = datetime.combine(datetime(1, 1, 1), sunrise)
    sunset_date = datetime.combine(datetime(1, 1, 1), sunset)
    night1 = (sunrise_date + timedelta(minutes=20)).time()
    night2 = (sunset_date + timedelta(minutes=20)).time()
    dusk1 = (sunrise_date - timedelta(minutes=20)).time()

    wallpaper_suffix = 'night'
    if (now > sunrise and now < sunset):
        wallpaper_suffix = 'day'
    elif(now > dusk1 and now < sunrise):
        wallpaper_suffix = 'dusk'
    elif(now > sunset and now < night2):
        wallpaper_suffix = 'dusk2'
    return get_wallpaper_name(wallpaper_suffix)
