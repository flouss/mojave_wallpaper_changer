# Mojave Wallpaper Changer

Tired of those "easy" tutorials to create a dynamic wallpaper similar to the MAC OSX 12 one?

Those tutorials involve eather creating a diaporama of those wallpaper (that needs to be activated in a specific time to make it work correctly) or worse using the windows event scheduler (if you're on windows).
I've decided to create an easy script that will change the wallpaper according to the time of day.

## Notes
* Location is found using your IP address and then looking into `http://ipinfo.io/json`. Using a VPN will surely make the wallpaper selection incorrect.
* Time for sunrise and sunset are looked up from your location using `https://sunrise-sunset.org/`.
* This script uses 4 wallpapers only.
* Wallpaper can be changed by replacing the files or changing the file names in the script.
* This script can be started using either the `mojave wallpaper changer.cmd` or by running
```sh
$ pythonw main.py
```